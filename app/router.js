import EmberRouter from '@ember/routing/router';
import config from './config/environment';

const Router = EmberRouter.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('node', { path : '/node/:node_id' });
  this.route('network');
  this.route('done');
});

export default Router;

import Route from '@ember/routing/route';

//set the dimensions and margins of the graph
var margin = { top: 10, right: 30, bottom: 30, left: 40 },
    width = 1000 - margin.left - margin.right,
    height = 1000 - margin.top - margin.bottom;

// append the svg object to the body of the page
var svg = d3.select("#d3")
    .append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
    .append("g")
    .attr("transform",
        "translate(" + margin.left + "," + margin.top + ")");

var link, node;
var mforce = 0.05;

var stringToColour = function(str) {
  var hash = 0;
  for (var i = 0; i < str.length; i++) {
    hash = str.charCodeAt(i) + ((hash << 5) - hash);
  }
  
  var colour = '#';
  for (var i = 0; i < 3; i++) {
    var value = (hash >> (i * 8)) & 0xFF;
    colour += ('00' + value.toString(16)).substr(-2);
  }
  return colour;
};

function getPathColor(s) {
    if(s) {
       return stringToColour(s);
    } else {
        return "#ff8080";
    }
}

function setLocation(node) {
    node.x = width/2;
    node.y = height/2;
    
    if(node.pathindex) {
        var pathcount = Object.keys(model.paths).length;
        var angle = node.pathindex/pathcount * 2 * Math.PI;
        node.x += Math.cos(angle) * width/2;
        node.y += Math.sin(angle) * height/2;
    }
}

var drag = simulation => {
  
  function dragstarted(d) {
    if (!d3.event.active) simulation.alphaTarget(0.3).restart();
    d.fx = d.x;
    d.fy = d.y;
  }
  
  function dragged(d) {
    d.fx = d3.event.x;
    d.fy = d3.event.y;
  }
  
  function dragended(d) {
    if (!d3.event.active) simulation.alphaTarget(0);
    d.fx = null;
    d.fy = null;
  }
  
  return d3.drag()
      .on("start", dragstarted)
      .on("drag", dragged)
      .on("end", dragended);
};

/*
model.callbacks.push((m) => {
	console.log("callback model " + m);
	apply();	
})*/

function apply() {
    // Let's list the force we wanna apply on the network
    var simulation = d3.forceSimulation(model.nodes)                 // Force algorithm is applied to data.model.nodes
        .force("link", d3.forceLink()                               // This force provides links between model.nodes
            .id(function(d) { return d.id; })                     // This provide  the id of a node
            .links(model.links)                                    // and this the list of links
        .strength((d) => {
           return mforce;
        }))
        .force("charge", d3.forceManyBody().strength( (d) => {
            return -5;
            //return -(100-d.order)/2 * mforce;
        }))         // This adds repulsion between model.nodes. Play with the -400 for the repulsion strength
        .force("x", d3.forceX(width / 2).strength(function(d) {
            return d.order/100 * mforce;
        }))
        .force("y", d3.forceY(height / 2).strength(function(d) {
            return d.order/100 * mforce;
        }))
/*       .force("pathorderx", d3.forceX((d) => {
            if(d.pathindex) {
                var pathcount = Object.keys(paths).length
                var angle = d.pathindex/pathcount * 2 * Math.PI;
                return Math.cos(angle) * width/2;
            } else {
                return width/2;
            }
        }).strength(function(d) {
            if(d.pathindex<3) {
                return 1 * mforce;
            } else {
                return 0;
            }
        }))
        .force("pathordery", d3.forceY((d) => {
            if(d.pathindex) {
                var pathcount = Object.keys(paths).length
                var angle = d.pathindex/pathcount * 2 * Math.PI;
                return Math.sin(angle) * height/2;
            } else {
                return height/2;
            }
        }).strength(function(d) {
            if(d.pathindex<3) {
                return 1 * mforce;
            } else {
                return 0;
            }
        }))
*/
        //.force("center", d3.forceCenter(width / 2, height / 2))     // This force attracts model.nodes to the center of the svg area
        .on("end", ticked);

    simulation.on("tick", () => {
        link
            .attr("x1", d => d.source.x)
            .attr("y1", d => d.source.y)
            .attr("x2", d => d.target.x)
            .attr("y2", d => d.target.y);
    
        node
            .attr("cx", d => d.x)
            .attr("cy", d => d.y);
      });


    // Initialize the model.nodes
    node = svg
        .selectAll("circle")
        .data(model.nodes)
        .enter()
        .append("circle")
        .attr("r", (d) => { 
            if(d.type=="leaf") {
                size = Math.sqrt(d.order); 
                if(size < 3) {
                    size = 3;
                }
                return size;
            } else {
                return 2;
            }
         })
        .style("fill", (d) => {
            if(d.type=="leaf") {
                return "#89b382";
            } else if(d.type=="path") {
                return getPathColor(d.pathid);
            } else {
                console.log("unknown type " + d.type + " in " + d.name);
                return "#ff0000";
            }
        })
        .attr("cx", () => { return width * Math.random(); })
        .attr("cy", () => { return height * Math.random(); })
        .attr("nodeid", (d) => { return d.id + " " + d.name; })
        .attr("pathindex", (d) => { return d.pathindex; })
        //.append("title").text((d) => { return d.name; } )
        .call(drag(simulation))
        ;
        
    d3.selectAll("circle"). filter((d, i) => {
        console.log("filter cirlce " + d.pathindex);
        return d.pathindex < 10;
    })
    //.style('fill', 'orange');
    
    .append("text").text(d => {
        console.log("text " + d.name);
        return d.name;
    }); 
        
    // Initialize the links
    link = svg
        .selectAll("line")
        .data(model.links)
        .enter()
        .append("line")
        .style("stroke", (l) => {
             if(l.type=="leaf") {
                return "#89b382";
            } else if(l.type=="path") {
                return getPathColor(l.pathid);
            } else {
                console.log("unknown type " + l.type + " in " + l.name);
                return "#ff0000";
            }           
        })
        .style("stroke-width", (l) => { 
            if(l.type=="leaf") {
                return 1;
            } else if(l.type=="path") {
                return 3;
            } else {
                console.log("unkown type " + l.type);
                return 1;
            }
         });
    
      invalidation.then(() => simulation.stop());
}


function ticked() {
    console.log("ticked");

    link
        .attr("x1", function(d) { return d.source.x; })
        .attr("y1", function(d) { return d.source.y; })
        .attr("x2", function(d) { return d.target.x; })
        .attr("y2", function(d) { return d.target.y; });

    node
        .attr("cx", function(d) { return d.x + 6; })
        .attr("cy", function(d) { return d.y - 6; });
}

// Toggle children on click.
function click(d) {
    if (!d3.event.defaultPrevented) {
        if (d.children) {
            d._children = d.children;
            d.children = null;
        } else {
            d.children = d._children;
            d._children = null;
        }
        update();
    }
}



export default Route.extend({
});

import Route from '@ember/routing/route';

var NodeRoute = {
		
};

export default Route.extend({

	model(nparams) {			
		var params = nparams;

		// scroll to top when link-to is used
		window.scrollTo(0,0);

		console.log("node route model " + params.node_id);
		
		return App.updateNodeData(params.node_id);
	},
	
	actions: {
		toggle(nodeid) {
			profile.toggleDone(nodeid);
			this.refresh();
		}
	}

});

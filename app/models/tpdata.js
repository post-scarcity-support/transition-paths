import DS from 'ember-data';
const { Model } = DS;

export default Model.extend({
	links: DS.attr(),
	nodes: DS.attr(),
	paths: DS.attr(),
	node_id: DS.attr()
});

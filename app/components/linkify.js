import Component from '@ember/component';
import { computed } from '@ember/object';

export default Component.extend({
	didInsertElement() {
	    Ember.run.scheduleOnce('afterRender', this, function() {
	    	$("p").linkify();
	    });
	},
});

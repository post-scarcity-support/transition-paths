var	REQUIRE_SAME_PATH_COUNT = 5;
var SAME_PATH_ADDITION_MAX = 100;

var App = {
		
};

function updateNodeDescriptions(nodes) {
	setTimeout(() => {
		for(i in nodes) {
			if(i!="_super") {
				var onode = nodes[i];
				tpdata.fetchNodeDescription(onode);
			}
		}
	}, 200);
};

function addPathsToList(list, node) {
	for(path in node.paths) {
		if(path!="_super" && !list.includes(path)) {
			list.push(path);
		}
	}
}

App.initializeFirstNodes = async function(node_id, d, m) {
	m.first_nodes = [];
	
	profile.reset();
	
	var addition = 0;
	
	var fngatheredpaths = [];

	var loopwithgatheredpaths = false;

	var highestlevel = await profile.getHighestLevel();
	console.log("first_nodes highestlevel " + highestlevel);
	while(m.first_nodes.length<5 && addition < SAME_PATH_ADDITION_MAX) {
		console.debug("first_nodes addition " + addition);

		for(i in d.nodes) {
			var fnode = d.nodes[i];

			//console.debug("first_nodes fnode " + fnode.isleaf + " order:" + fnode.order);

			if(fnode.isleaf && fnode.order >= highestlevel/2) {
				for(var p in fnode.paths) {
					var l = await profile.getLevel(p);
					var comparel = Number(l) + addition;
	
					//console.debug("first_nodes compare " + comparel);

					if(fnode.order<=comparel || fnode.order<=comparel) {
						var isdone = profile.isDone(fnode.id);

						var path_already_gathered = false;
						for(gatheringpath in fnode.paths) {
							if(gatheringpath!="_super" && fngatheredpaths.includes(gatheringpath)) {
								path_already_gathered = true;
							}
						}

						if((!path_already_gathered || loopwithgatheredpaths) && !isdone && ! m.first_nodes.includes(fnode)) {
							addPathsToList(fngatheredpaths, fnode);
							
							console.log("first nodes added " + fnode.id + " level:" + l + " comparel:" + comparel + " node level: " + fnode.order);
							m.first_nodes.push(fnode);
						}
					}
				}
			}
		}
		
		addition+=10;
		
		if(!loopwithgatheredpaths && addition>=SAME_PATH_ADDITION_MAX && m.first_nodes.length < REQUIRE_SAME_PATH_COUNT) {
			loopwithgatheredpaths = true;
			addition = 0;
			console.log("Not enough nodes in first_nodes (count:" + m.first_nodes.length + ").");
			console.log("loop again without caring about paths")
		}
	}
	
	m.first_nodes.sort((a, b) => {
		return a.order - b.order;
	});
	
	updateNodeDescriptions(m.first_nodes);
};

//FIXME OMG this turned in to a mess!
App.initializeSamePaths = async function(node_id, d, m) {
	console.log("initializeSamePaths " + JSON.stringify(node));
	
	m.same_path = [];

	var addition = 0;
	
	var gatheredpaths = [];
	var loopwithgatheredpaths = false;
	
	var highestlevel = await profile.getHighestLevel();

	while(m.same_path.length<REQUIRE_SAME_PATH_COUNT && addition<SAME_PATH_ADDITION_MAX) {
		for(i in d.nodes) {
			var onode = d.nodes[i];
			if(onode.id!=m.node.id) {
				for(var opath in onode.paths) {
					if(opath!="_super" && (opath in m.node.paths || (loopwithgatheredpaths && gatheredpaths.includes(opath)))) {
						// gathering all the close paths for later
						// will loop again with all gathered paths if not enough nodes are found
						for(gatheringpath in onode.paths) {
							if(!loopwithgatheredpaths && gatheringpath!="_super" && !gatheredpaths.includes(gatheringpath)) {
								gatheredpaths.push(gatheringpath);
							}
						}
						
						var l = await profile.getLevel(opath);
						var comparel = Number(l)+addition;
						if((onode.order<=1 || Number(onode.order)<=comparel) && onode.order >= highestlevel/2) {
							var isdone = profile.isDone(onode.id);
							console.debug("checking path done:" + isdone + " json: " + JSON.stringify(onode));
							if(!isdone && !m.same_path.includes(onode)) {
								console.log("same_path added " + JSON.stringify(onode) 
										+ " when opath:" + opath 
										+ " because m.paths:" + (opath in m.node.paths) 
										+ " gatheredpaths:" + (gatheredpaths.includes(opath)));
								m.same_path.push(onode);
							}
						}
					}
				}
			}
		}
		addition+=10;
		
		if(loopwithgatheredpaths) {
			highestlevel-=5;			
		}
		
		if(!loopwithgatheredpaths && addition>=SAME_PATH_ADDITION_MAX && m.same_path.length < REQUIRE_SAME_PATH_COUNT) {
			loopwithgatheredpaths = true;
			addition = 0;
			console.log("Not enough nodes in same path (count:" + m.same_path.length + "). Adding from close paths.");
			console.log("loop again with gatheredpaths " + gatheredpaths)
		}
	}
	
	if(m.same_path.length < REQUIRE_SAME_PATH_COUNT) {
		console.log("Not enough nodes in same path (count:" + m.same_path.length + "). Adding from other.");
		console.log("gathered paths " + gatheredpaths);
	}
	
	addition = 0;
	while(m.same_path.length < REQUIRE_SAME_PATH_COUNT && addition < SAME_PATH_ADDITION_MAX) {
		for(i in m.first_nodes) {
			if(isValidIndex(i)) {
				var onode = m.first_nodes[i];
				if(onode.id!=m.node.id && !m.same_path.includes(onode)) {
					var dorder = Math.abs(onode.order - node.order);
					console.debug("MORE! checking " + i + " node:" + onode.id + " dorder " + dorder + " comparing to addition " + addition);
					if(dorder <= addition && !m.same_path.includes(onode)) {
						m.same_path.push(onode);
					}
				}
			}
		}
		addition+=10;
	}
	
	console.log("same path length " + m.same_path.length);
	
	m.same_path.sort((a, b) => {
		return a.order - b.order;
	});

	console.log("initializeSamePaths done");
	
	updateNodeDescriptions(m.same_path);
	updateNodeDescriptions(m.first_nodes);
		
	return m;	
};

App.initializeDoneNodes = async function(d, m) {
	console.log("initializeDoneNodes");
	
	m.done_nodes = [];
	m.not_done_nodes = [];
	
	for(i in d.nodes) {
		var fnode = d.nodes[i];
		if(fnode.isleaf) {
			if(profile.isDone(fnode.id)) {
				m.done_nodes.push(fnode);
			} else {
				m.not_done_nodes.push(fnode);			
			}
		}
	}
	
	m.done_nodes.sort((a, b) => {
		return a.order - b.order;
	});

	m.not_done_nodes.sort((a, b) => {
		return a.order - b.order;
	});

	return m;
};

App.updateNodeData  = async function(node_id) {
	console.log("getting data " + node_id);
		
	var d = await tpdata.get();
	var m = {};
	var node = d.nodes[node_id]; 
	
	m.data = d;
	m.node = node;
	m.first_node = d.nodes[0];
	
	await App.initializeFirstNodes(node_id, d, m);

	if(node) {
		var sp = await App.initializeSamePaths(node_id, d, m);
		m.isdone = profile.isDone(node_id);
		m.same_paths = sp.same_paths;
				
		if(!node.desc) {
			console.log("loading node desc");
			m.desc = "Loading...";
			try {
				await tpdata.fetchNodeDescription(node);
				m.desc = await tpdata.getNodeDescription(node);
			} catch(e) {
				console.log("ERROR handleRouteData " + e.name);
				console.log("ERROR handleRouteData " + e.message);
				console.log("ERROR handleRouteData " + e.stack);
			};
		}
	}

	m = await App.initializeDoneNodes(d, m);

	m.profile = profile.data;
	m.profilejson = JSON.stringify(profile.data);

	return m;
};

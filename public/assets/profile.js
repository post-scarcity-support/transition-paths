console.log("Initializing profile");

var profile = {
	
};

profile.reset = () => {
	profile.data = {};
	profile.data.levels = {};

	if(("" + localStorage.tpdone) != 'undefined') {
		profile.data.done = JSON.parse(localStorage.tpdone);
	} else {
		console.log("resetting storage");
		profile.data.done = {};
		profile.store();
	}
}

profile.store = () => {
	localStorage.tpdone = JSON.stringify(profile.data.done);
};

profile.reset();

profile.getHighestLevel = async() => {
	var d = await tpdata.get();
	var level = 0;
	for(inode in d.nodes) {
		if(profile.isDone(inode)) {
			var node = d.nodes[inode];
			if(node.order > level) {
				level = node.order;
			}
		}
	}
	return level;
};

profile.getLevel = async (pathid) => {
	if(profile.data.levels[pathid]) {
		return profile.data.levels[pathid];
	}
	
	var d = await tpdata.get();
	var level = 0;
	for(inode in d.nodes) {
		if(profile.isDone(inode)) {
			var node = d.nodes[inode];
			if(node.paths && pathid in node.paths) {
				level = node.order;
			}			
		}
	}

	profile.data.levels[pathid] = level;
	
	return Number(level);
};

profile.toggleDone = (taskid) => {
	console.log("Profile toggling " + taskid);
	profile.data.done[taskid] = !profile.isDone(taskid);
	profile.store();
	profile.reset();	
};

profile.setDone = (taskid) => {
	profile.data.done[taskid] = true;
	profile.store();
	profile.reset();
};

profile.setUnDone = (taskid) => {
	profile.data.done[taskid] = false;
	profile.store();
	profile.reset();
};

profile.isDone = (taskid) => {
	var value = profile.data.done[taskid];
	if(("" + value)=="undefined") {
		return false;
	}
	return value;
};

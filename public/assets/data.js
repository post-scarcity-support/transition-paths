var dataurl = "https://post-scarcity-support.gitlab.io/transition-paths-data/";
//var dataurl = "file:///home/juuso/git/transition-paths-data/_site";
//var dataurl = "";

var nodedescriptions = {};

var pathindexes = [];
var groups = {
    "A": [ "healing from consumerasim", "green movement", "self-sustainability", "Local sharing and help", "Local common ownership"]
};

var tpdata = {
};

function isDefined(variable) {
	if(("" + variable) == "undefined") {
		return false;
	}
	
	if (typeof variable !== 'undefined') {
		return true;
	}
	return false;
}

function isValidIndex(i) {
	return isDefined(i) && i!="_super";
}

function waitData() {
	return new Promise(resolve => {
		
	});
}

tpdata.get = async function() {
	while(tpdata.loading) {
		await new Promise((resolve) => {
			setTimeout(resolve, 100);	
		});
	}
	
	if(tpdata.loaded) {
		return tpdata;
	} else if (!tpdata.loading){
		tpdata.loading = true;
		console.log("Load data");
		
		try {		
			var data = await $.get(dataurl + "/data.json");
			console.log("data loaded");
			console.log("data " + JSON.stringify(data));
		    //var data = JSON.parse(sdata);
		    //console.log("data parsed");
		    
		    var nmodel = processData(data);
		    tpdata.nodes = nmodel.nodes;
		    tpdata.links = nmodel.links;
		    tpdata.paths = nmodel.paths;
		    tpdata.loaded = true;
		    tpdata.loading = false;
	
		    return tpdata;
		} catch(e) {
			console.error("ERROR loading or parsing data " + e.name);
			console.error("ERROR loading or parsing data " + e.message);
			console.error("ERROR loading or parsing data " + e.stack);
		}
		
	} else {
		throw new Error("ERROR data not loaded");
	}		
}

tpdata.fetchNodeDescription = async (node) => {
	if(!nodedescriptions[node.id]) {
		console.log("Loading node description " + node.id);
		desc = await $.get(dataurl + "json/" + node.id + ".json");
		var metadata = desc.raw;
		//delete desc.raw;
		console.log("node[" + node.id + "] desc " + JSON.stringify(desc));		
		nodedescriptions[node.id] = desc;
	} else {
		console.debug("Already loaded description " + node.id);
	}
	
	return node;
}

tpdata.getNodeDescription = (node) => {
	node.desc = nodedescriptions[node.id];
	return nodedescriptions[node.id];
}

function getPathID(d) {
    return d.replace(/[]/g, "_");
}

function getPathIDn(d, n) {
    return getPathID(d) + "_" + n;
}

function isColumnGroupSelected(k) {
	if(k=="_super") {
		return false;
	}
    return true;
    // console.log("isColumnGroupSelected " + k)
    // return groups['A'].includes(k);
}

function sortNodesByOrder(model) {
	var nodes = model.nodes;
	
}

function processData(data) {
	var model = {
	        "nodes": {},
	        "links": [],
			"paths": {}
	    };
	
    // every path has a node on every level
	for(i in data) {
		var d = data[i];
		console.debug("processData i:" + i + " d:" + JSON.stringify(d));
        // d.order = (Math.ceil(d.order/5)) * 5;
        console.debug("processData order:" + d.order);
        $(d.tags).each((k, tag) => {
        	console.debug("processdata tags k:" + k);
        	console.debug("Processdata tags tag " + JSON.stringify(tag));
        	if (isColumnGroupSelected(tag)) {
                // console.log("PATH " + d.name + " is " + k);
                var pathid = getPathID(tag);
                if (!model.paths[pathid]) {
                    model.paths[pathid] = {};
                    pathindexes[pathid] = Object.keys(pathindexes).length;
                    console.log("new path found called " + pathid);
                }

                if (model.paths[pathid][d.order] == null) {
                    pathnode = {
                        "name": tag,
	                    "order": Number(d.order),
	                    "pathid": pathid,
	                    "type": "path",
	                    "pathorder": Object.keys(model.paths[pathid]).length,
	                    "pathindex": pathindexes[pathid]
	                };
	                                        
	                model.paths[pathid][d.order] = pathnode;
	                pathnode.id = getPathIDn(pathid, d.order);
	                console.debug("PATH new path node " + pathnode.id);

                    model.nodes[pathnode.id] = pathnode;
                }
            }
        });
    }

    // link every path together
    pathidcount = 0;
    for (pathid in model.paths) {
        var lastnode = null;
        for (inode in model.nodes) {
            node = model.nodes[inode];
            
            if (node.pathid == pathid) {
                if (lastnode != null) {
                    console.debug("PATH LINK link " + lastnode.id + " -> " + node.id + " pathid:" + pathid);
                    model.links.push({ "source": lastnode.id, "target": node.id, "type": "path", "pathid": pathid });
                }
                lastnode = node;
            }
        }

        pathidcount++;
    }

    // console.log("nodes " + JSON.stringify(model.nodes));
 
    // leafs
    for(i in data) {
    	var d = data[i];
        if (d.order) {
            console.debug("LEAF " + d.name);
            console.debug("LEAF " + JSON.stringify(d));

            tasknode = {};
            tasknode.id = d.id; 
            tasknode.name = d.name;
            tasknode.order = Number(d.order);
            tasknode.path = d.path;
            tasknode.img = d.img;
            tasknode.excerpt = d.excerpt;
            tasknode.type = "leaf";
            tasknode.paths = {};
            tasknode.isleaf = true;
            
            var linked = false;
            for(k in d.tags) {
            	var tag = d.tags[k];
                if (isValidIndex(k) && isDefined(tag) && isColumnGroupSelected(tag)) {
                	console.debug("Leafs tag " + tag);
                    if(!linked) {
                        // node not yet added
                        console.debug("push " + tasknode.name);
                        model.nodes[tasknode.id] = tasknode;
                    }

                    tasknode.paths[getPathID(tag)] = true;

                    linked = true;
                    console.debug("LEAF should link \"" + d.name + "\" to " + getPathIDn(tag, d.order) );
                    model.links.push({ "source": tasknode.id, "target": getPathIDn(tag, d.order), "type": "leaf" });
                    
                    tasknode.linksource = true;
                }
            };

            // model.links.push({ "source": tasknode.id, "target": root.id,
			// "type": "root" });
            
            if(!linked) {
                console.log("Oh no! " + d.name + " not linked to anything");
            }
        }
    };

    for(i in model.nodes) {
        n = model.nodes[i];
        
        if(n && !n.linksource && n.type != "path") {
            console.log("not a link source " + n.name + " type:" + n.type);
            console.log("n " + JSON.stringify(n));
        }
    }

    sortNodesByOrder(model);
    
    console.log("----- FINAL DATA -----");
    console.log(JSON.stringify(model));
    
    return model;
}
